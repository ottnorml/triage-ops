# frozen_string_literal: true

require_relative '../lib/container_security_helper'

Gitlab::Triage::Resource::Context.include ContainerSecurityHelper
