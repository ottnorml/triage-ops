# GitLab triage operations

*This is a prototype. The goal is to add immediate value to the GitLab Engineering function, while determining if it adds value to customers. If so, we will work with Product Management to [bring this functionality to GitLab the product](https://gitlab.com/groups/gitlab-org/-/epics/636).*

Triage operations for GitLab issues and merge requests, with two strategies: reactive & scheduled operations.

## The bot

We're using [@gitlab-bot](https://gitlab.com/gitlab-bot) as the user to run
triage operations. The credentials could be found in the shared 1Password
vault.

The same bot is also used in the following projects:

* https://gitlab.com/gitlab-org/release-tools
* https://gitlab.com/gitlab-org/async-retrospectives
* https://gitlab.com/gitlab-com/gl-infra/triage-ops

## Reactive operations

See the dedicated [Reactive operations](doc/reactive/index.md) documentation.

## Scheduled operations

See the dedicated [Scheduled operations](doc/scheduled/index.md) documentation.

## Development

### Install gems

```
bundle install
```

### Run tests

You can run the test suite with `bundle exec rspec`.

Guard is also supported (see the [`Guardfile`](Guardfile)). Install Guard with `gem install guard`, then start it with `guard`.
