resource_rules:
  merge_requests:
    summaries:
      - name: Identify idle and recently merged ~"Community contribution" MRs
        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            title: |
              #{Date.today.iso8601} - Community contributions report
            summary: |
              This report contains four groups of merge requests:

                1. Merge requests from new unique monthly contributors, but idle for 7 days.
                1. Merge requests idle for 28 days, excluding the ones listed in 1.
                1. Merge requests awaiting a response from the author.
                1. Merge requests merged in the past week.

              Assignees should go through groups 1. and 2. and process the MRs in them. All MRs in these groups are actionable and require a response. After processing an MR the checkbox on the left should be checked.

              When going through the list of MRs please do one of the following:
                - If there is a reviewer assigned and the last comment is from the reviewer, ping the author in a comment and label the merge request with ~"🏓".
                - If there is a reviewer assigned and the last comment is from the author, ping the reviewer in a comment.
                - If there is no reviewer assigned, use https://gitlab-org.gitlab.io/gitlab-roulette/ to find a reviewer and assign the merge request to them.
              #{ short_team_summary(title: "## Community Contribution Report", **group_quality_community) }

              ----

              Job URL: #{ENV['CI_JOB_URL']}

              This report was generated from [this policy](#{ENV['CI_PROJECT_URL']}/blob/#{ENV['CI_DEFAULT_BRANCH']}/policies/groups/gitlab-org/quality/community-contribution-mr-report.yml).
        rules:
          - name: 1. Inactive new unique monthly contributor merge requests
            conditions:
              state: opened
              labels:
                - Community contribution
                - 1st contribution
              ruby: |
                IdleMrHelper.idle?(resource, days: 7)
            actions:
              summarize:
                item: |
                  - [ ] #{IdleMrHelper.days_since_last_human_update(resource)} since last update | #{resource[:web_url]} | {{labels}}
                summary: |
                  ### #{resource[:items].lines.size} inactive (updated more than 7 days ago) merge requests from new unique monthly contributors

                  {{items}}
          - name: 2. Idle ~"Community Contribution" merge requests awaiting GitLab response
            conditions:
              state: opened
              labels:
                - Community contribution
                - idle
              forbidden_labels:
                - 🏓
            actions:
              summarize:
                item: |
                  - [ ] #{IdleMrHelper.days_since_last_human_update(resource)} since last update | #{resource[:web_url]} | {{labels}}
                summary: |
                  ### #{resource[:items].lines.size} idle (updated more than 28 days ago) community merge requests

                  {{items}}
          - name: 3. Idle ~"Community Contribution" merge requests awaiting author response
            conditions:
              state: opened
              labels:
                - Community contribution
                - idle
                - 🏓
            actions:
              summarize:
                item: |
                  - [ ] #{IdleMrHelper.days_since_last_human_update(resource)} since last update | #{resource[:web_url]} | {{labels}}
                summary: |
                  ### #{resource[:items].lines.size} idle (updated more than 28 days ago) community merge requests

                  {{items}}
          - name: 4. Merged ~"Community contribution" merge requests
            conditions:
              state: merged
              labels:
                - Community contribution
              date:
                attribute: merged_at
                condition: newer_than
                interval_type: days
                interval: 7
            actions:
              summarize:
                item: |
                  `{{author}}`,`#{resource[:web_url]}`,{{labels}}
                summary: |
                  ### Merged ~"Community contribution" in past 7 days

                  #{ mrs = merge_requests_from_line_items(resource[:items]); nil }
                  #{ tabulate_merge_requests_by_author(mrs) }
