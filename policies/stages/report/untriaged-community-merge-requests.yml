.common_limits: &common_limits
  limits:
    most_recent: 100

.common_summary_rule_actions: &common_summary_rule_actions
  item: |
    - [ ] #{full_resource_reference}+ {{labels}}

resource_rules:
  merge_requests:
    rules:
      - name: Untriaged community merge requests requiring initial triage
        conditions:
          state: opened
          labels:
            - Community contribution
          forbidden_labels:
            - "group::not_owned"
          ruby: |
            return false if has_department_label?

            !(has_group_label? && has_stage_label?)
        <<: *common_limits
        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            <<: *common_summary_rule_actions
            title: |
              #{Date.today.iso8601} Untriaged community merge requests requiring initial triage
            summary: |
              Hi merge request coaches,

              Here is a list of the ~"Community contribution" merge requests that do not have a stage and group label, we would like to ask you to:

              1. If missing, add a [type label](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#type-labels).
              1. If missing, add a [group label](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#group-labels).
              1. If missing, add a [stage label](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#stage-labels).
              1. Add relevant [category](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#category-labels) labels to facilitate automatic addition of stage and group labels.

              For the merge requests triaged please check off the box in front of the given merge request.

              Once you've triaged all the merge requests assigned to you, you can unassign and unsubscribe yourself via these quick actions:

              ```
              /done
              /unassign me
              /unsubscribe
              ```

              **When all the checkboxes are done, close the issue, and celebrate!** :tada:

              #{ coaches = merge_request_coaches; nil }
              #{ distribute_and_display_items_per_triager(resource[:items].lines(chomp: true).last(coaches.size * 2), coaches) }

              ---

              Job URL: #{ENV['CI_JOB_URL']}

              This report was generated from [this policy](#{ENV['CI_PROJECT_URL']}/blob/#{ENV['CI_DEFAULT_BRANCH']}/policies/stages/report/untriaged-community-merge-requests.yml)

              /label ~"triage report"
