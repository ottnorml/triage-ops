# frozen_string_literal: true

require_relative '../triage/processor'

module Triage
  class InfradevLabelTransition < Processor
    INFRADEV_LABEL = 'infradev'
    ENGINEERING_ALLOCATION_LABELS = [
      'Engineering Allocation',
      'Eng-Consumer::Infrastructure',
      'Eng-Producer::Development'
    ].freeze

    react_to 'issue.open', 'issue.update'

    def applicable?
      event.from_gitlab_org? &&
        engineering_allocation_labels_need_to_be_updated?
    end

    def process
      if infradev_label_removed?
        post_quick_action('unlabel')
      else
        post_quick_action('label')
      end
    end

    private

    def engineering_allocation_labels_need_to_be_updated?
      if infradev_label_present?
        missing_engineering_allocation_labels?
      elsif infradev_label_removed?
        any_engineering_allocation_label_set?
      else
        false
      end
    end

    def infradev_label_present?
      event.label_names.include?(INFRADEV_LABEL)
    end

    def infradev_label_removed?
      event.removed_label_names.include?(INFRADEV_LABEL)
    end

    def missing_engineering_allocation_labels?
      engineering_allocation_labels_set.size != ENGINEERING_ALLOCATION_LABELS.size
    end

    def any_engineering_allocation_label_set?
      engineering_allocation_labels_set.any?
    end

    def engineering_allocation_labels_set
      ENGINEERING_ALLOCATION_LABELS & event.label_names
    end

    def post_quick_action(action)
      add_comment <<~MARKDOWN.chomp
        /#{action} #{ENGINEERING_ALLOCATION_LABELS.map { |label| %(~"#{label}") }.join(' ')}
      MARKDOWN
    end
  end
end
