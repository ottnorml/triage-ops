# frozen_string_literal: true

require_relative '../triage/processor'

module Triage
  # This hints and removes `~"backstage [DEPRECATED]"` label because we don't
  # want to use this label anymore.
  class BackstageLabel < Processor
    BACKSTAGE_LABELS = [
      'backstage',
      'backstage [DEPRECATED]'
    ].freeze

    react_to 'issue.*', 'merge_request.*'

    def applicable?
      event.from_gitlab_org? &&
        backstage_label_added?
    end

    def process
      post_deprecation_message
    end

    private

    def backstage_label_added?
      !!backstage_label_added
    end

    def backstage_label_added
      (event.added_label_names & BACKSTAGE_LABELS).first
    end

    def post_deprecation_message
      add_comment <<~MARKDOWN.chomp
        Hey @#{event.event_user_username}, ~"#{backstage_label_added}" is being deprecated in favor of ~"type::feature", ~"feature::addition", ~"type::maintenance", ~"type::tooling", ~"tooling::pipelines", and ~"tooling::workflow" to improve the identification of these type of changes.
        Please see https://about.gitlab.com/handbook/engineering/metrics/#data-classification for further guidance.
        /unlabel ~"#{backstage_label_added}"
      MARKDOWN
    end
  end
end
