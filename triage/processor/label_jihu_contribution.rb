# frozen_string_literal: true

require_relative '../triage/processor'

module Triage
  class LabelJiHuContribution < Processor
    LABEL_NAME = 'JiHu contribution'

    react_to 'merge_request.open'

    def applicable?
      event.jihu_contributor?
    end

    def process
      add_comment(%Q{/label ~"#{LABEL_NAME}"})
    end
  end
end
