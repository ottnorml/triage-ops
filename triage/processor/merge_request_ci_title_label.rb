# frozen_string_literal: true

require_relative '../triage/processor'

module Triage
  class MergeRequestCiTitleLabel < Processor
    Label = Struct.new(:name) do
      def to_s
        %Q|#{LABEL_PREFIX}"#{name}"|
      end
    end

    LABEL_PREFIX = '~'
    RUN_ALL_RSPEC_LABEL = 'pipeline:run-all-rspec'
    RUN_AS_IF_FOSS_LABEL = 'pipeline:run-as-if-foss'
    UPDATE_CACHE_LABEL = 'pipeline:update-cache'
    SKIP_RSPEC_FAIL_FAST_LABEL = 'pipeline:skip-rspec-fail-fast'

    CI_LABEL_TITLE_MAP = {
      RUN_ALL_RSPEC_LABEL => /RUN ALL RSPEC/,
      RUN_AS_IF_FOSS_LABEL => /RUN AS-IF-FOSS/,
      UPDATE_CACHE_LABEL => /UPDATE CACHE/,
      SKIP_RSPEC_FAIL_FAST_LABEL => /SKIP RSPEC FAIL-FAST/
    }

    react_to 'merge_request.*'

    def applicable?
      from_allowed_project? &&
        event.resource_open? &&
        merge_request_title_match_ci_flag? &&
        ci_labels_missing?
    end

    def process
      add_comment <<~MARKDOWN.chomp
        /label #{labels_for_ci_title.join(' ')}
      MARKDOWN
    end

    def merge_request_title_match_ci_flag?
      labels_for_ci_title.any?
    end

    def ci_labels_missing?
      (labels_for_ci_title.map(&:name) - current_ci_labels).any?
    end

    def labels_for_ci_title
      @labels_for_ci_title ||= CI_LABEL_TITLE_MAP.each_with_object([]) do |(ci_label, ci_flag_regex), new_labels|
        new_labels << Label.new(ci_label) if ci_flag_regex.match?(event.title)
      end
    end

    private

    def from_allowed_project?
      event.from_gitlab_org_gitlab? ||
      from_alt_project?
    end

    def from_alt_project?
      event.with_project_id?(alt_project_id)
    end

    def alt_project_id
      ENV['ALT_PROJECT_ID_FOR_CI_TITLE_LABEL'].to_i
    end

    def current_ci_labels
      (event.label_names & CI_LABEL_TITLE_MAP.keys)
    end
  end
end
