# frozen_string_literal: true

require_relative '../triage/processor'

module Triage
  # This adds default labels upon resource closing/merging.
  class DefaultLabelUponClosing < Processor
    react_to 'issue.close', 'merge_request.close', 'merge_request.merge'

    MAINTENANCE_TYPE_LABELS = %w[security documentation].freeze

    def applicable?
      event.from_gitlab_org? &&
        !event.type_label_set? &&
        maintenance_type_labels_can_be_set?
    end

    def process
      default_label_upon_closing
    end

    private

    def default_label_upon_closing
      add_comment('/label ~"type::maintenance"')
    end

    def maintenance_type_labels_can_be_set?
      (event.label_names & MAINTENANCE_TYPE_LABELS).any?
    end
  end
end
