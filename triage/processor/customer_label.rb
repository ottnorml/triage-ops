# frozen_string_literal: true

require_relative '../triage/processor'

module Triage
  # This adds a `~customer` label whenever the comment contains a link to a
  # customer support ticket link.
  class CustomerLabel < Processor
    react_to 'issue.note', 'merge_request.note'

    def applicable?
      event.from_gitlab_org? &&
        has_customer_link?
    end

    def process
      add_customer_label
    end

    private

    def has_customer_link?(new_comment = event.new_comment)
      new_comment.include?('gitlab.zendesk.com') ||
        new_comment.include?('gitlab.my.salesforce.com')
    end

    def add_customer_label
      add_comment('/label ~customer')
    end
  end
end
