# frozen_string_literal: true

require_relative '../triage/processor'

module Triage
  class LegalDisclaimerOnDirectionResources < Processor
    DIRECTION_LABEL = 'direction'
    LEGAL_DISCLAIMER_VERSION = 3
    LEGAL_DISCLAIMER_TAG = "<!-- triage-serverless v#{LEGAL_DISCLAIMER_VERSION} PLEASE DO NOT REMOVE THIS SECTION -->"
    LEGAL_DISCLAIMER_TAG_REGEX = /#{Regexp.escape(LEGAL_DISCLAIMER_TAG).sub(/\\ v#{LEGAL_DISCLAIMER_VERSION}/, '( v)?(?<version>\d+)?')}/.freeze
    LEGAL_DISCLAIMER_REGEX = /#{LEGAL_DISCLAIMER_TAG_REGEX}\n(?<disclaimer_text>.+)\n#{LEGAL_DISCLAIMER_TAG_REGEX}/m.freeze
    LEGAL_DISCLAIMER_TEXT = <<~MESSAGE
      *This page may contain information related to upcoming products, features and functionality.
      It is important to note that the information presented is for informational purposes only, so please do not rely on the information for purchasing or planning purposes.
      Just like with all projects, the items mentioned on the page are subject to change or delay, and the development, release, and timing of any products, features, or functionality remain at the sole discretion of GitLab Inc.*
    MESSAGE
    LEGAL_DISCLAIMER = "#{LEGAL_DISCLAIMER_TAG}\n#{LEGAL_DISCLAIMER_TEXT}#{LEGAL_DISCLAIMER_TAG}"

    react_to 'issue.open', 'issue.update'

    def applicable?
      event.from_gitlab_org? &&
        labelled_direction? &&
        !description_includes_latest_legal_disclaimer?
    end

    def process
      if description_includes_any_legal_disclaimer?
        update_legal_disclaimer
      else
        add_legal_disclaimer
      end
    end

    private

    def labelled_direction?
      event.label_names.include?(DIRECTION_LABEL)
    end

    def description_includes_any_legal_disclaimer?
      LEGAL_DISCLAIMER_REGEX.match?(resource_description)
    end

    def description_includes_latest_legal_disclaimer?
      LEGAL_DISCLAIMER_VERSION == current_legal_disclaimer_version
    end

    def current_legal_disclaimer_version
      matches = LEGAL_DISCLAIMER_REGEX.match(resource_description)
      return unless matches

      matches[:version].to_i
    end

    def resource_description
      event.description
    end

    def update_legal_disclaimer
      Triage.api_client.edit_issue(event.project_id, event.iid, description: resource_description.gsub(LEGAL_DISCLAIMER_REGEX, LEGAL_DISCLAIMER))
    end

    def add_legal_disclaimer
      Triage.api_client.edit_issue(event.project_id, event.iid, description: "#{resource_description}\n#{LEGAL_DISCLAIMER}")
    end
  end
end
