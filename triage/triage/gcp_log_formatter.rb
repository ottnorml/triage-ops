# frozen_string_literal: true

require 'ougai/formatters/base'
require 'active_support'

module Triage
  # Extending https://github.com/tilfin/ougai/blob/3f5d9b8ecd6655b58a02b14208e84e91cee862f1/lib/ougai/formatters/bunyan.rb#L26
  class GcpLogFormatter < Ougai::Formatters::Bunyan
    WEB_ENDPOINT = "https://triage-ops.gitlab.com"
    HTTP_KEYS = %i[method path status remote_addr].freeze

    def _call(severity, time, progname, data)
      http_data = data.slice(*HTTP_KEYS)
      predefined_keys = HTTP_KEYS + %i[msg]
      payload_data = data.except(*predefined_keys)

      payload = {
        name: progname || @app_name,
        hostname: @hostname,
        pid: $$,
        severity: gcp_severity(severity),
        time: time,
        message: data[:msg]
      }
      payload[:httpRequest] = gcp_http_request(**http_data) if data.key?(:method)
      dump(payload.merge(payload_data))
    end

    def gcp_severity(severity)
      case severity
      when 'TRACE'
        'DEBUG'
      when 'WARN'
        'WARNING'
      when 'FATAL'
        'CRITICAL'
      when 'UNKNOWN'
        'DEFAULT'
      else
        severity
      end
    end

    def gcp_http_request(method:, path:, status:, remote_addr:)
      {
        requestMethod: method,
        requestUrl: "#{WEB_ENDPOINT}#{path}",
        status: status,
        remoteIp: remote_addr
      }
    end
  end
end
