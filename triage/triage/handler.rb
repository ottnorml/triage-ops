# frozen_string_literal: true

require_relative '../processor/availability_priority'
require_relative '../processor/backstage_label'
require_relative '../processor/command_request_review'
require_relative '../processor/customer_label'
require_relative '../processor/default_label_upon_closing'
require_relative '../processor/deprecated_label'
require_relative '../processor/doc_community_contribution'
require_relative '../processor/engineering_allocation_labels_reminder'
require_relative '../processor/hackathon_label'
require_relative '../processor/infradev_label_transition'
require_relative '../processor/label_jihu_contribution'
require_relative '../processor/label_inference'
require_relative '../processor/legal_disclaimer_on_direction_resources'
require_relative '../processor/merge_request_ci_title_label'
require_relative '../processor/merge_request_help'
require_relative '../processor/new_pipeline_on_approval'
require_relative '../processor/reactive_labeler'
require_relative '../processor/reactive_reviewer'
require_relative '../processor/thank_community_contribution'
require_relative '../processor/ux_community_contribution'
require_relative '../processor/pajamas_missing_workflow_label_or_weight'
require_relative '../processor/code_review_experience_feedback'
require_relative '../processor/code_review_experience_slack'
require_relative '../processor/plan_ux_mr_review_support'
require_relative '../processor/seeking_community_contributions_label'
require_relative '../processor/remind_merged_mr_deviating_from_guidelines'
require_relative '../processor/breaking_change_comment'
require_relative 'listener'

module Triage
  class Handler
    DEFAULT_PROCESSORS = [
      RemindMergedMrDeviatingFromGuideline,
      DefaultLabelUponClosing,
      AvailabilityPriority,
      BackstageLabel,
      CommandRequestReview,
      CustomerLabel,
      DeprecatedLabel,
      DocCommunityContribution,
      EngineeringAllocationLabelsReminder,
      HackathonLabel,
      InfradevLabelTransition,
      LabelJiHuContribution,
      LabelInference,
      LegalDisclaimerOnDirectionResources,
      MergeRequestCiTitleLabel,
      MergeRequestHelp,
      NewPipelineOnApproval,
      ReactiveLabeler,
      ReactiveReviewer,
      ThankCommunityContribution,
      UxCommunityContribution,
      PajamasMissingWorkflowLabelOrWeight,
      CodeReviewExperienceFeedback,
      CodeReviewExperienceSlack,
      PlanUxMrReviewSupport,
      SeekingCommunityContributionsLabel,
      BreakingChangeComment
    ].freeze

    Result = Struct.new(:message, :error, :duration)

    def initialize(event, processors: DEFAULT_PROCESSORS)
      @event = event
      @processors = processors
    end

    def process
      results = Hash.new { |h, k| h[k] = Result.new }

      listeners[event.key].each do |processor|
        start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC, :float_second)
        results[processor.name].message = processor.triage(event)
      rescue => e
        results[processor.name].error = e
      ensure
        results[processor.name].duration = (Process.clock_gettime(Process::CLOCK_MONOTONIC, :float_second) - start_time).round(5)
      end

      results.select { |processor, result| result.message || result.error }
    end

    private

    attr_reader :event, :processors

    def listeners
      @listeners ||= processors.each_with_object(Hash.new { |h, k| h[k] = [] }) do |processor, result|
        processor.listeners.each do |listener|
          result[listener.event] << processor
        end
      end
    end
  end
end
