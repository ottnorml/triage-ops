# frozen_string_literal: true

require 'rack'

require_relative '../triage/error'
require_relative '../triage/handler'
require_relative '../triage/sentry'
require_relative '../triage'

module Triage
  module Rack
    class Processor
      def self.call(env)
        new(env).call
      end

      def initialize(env)
        @event = env[:event]
        @logger = env[::Rack::RACK_LOGGER]
      end

      def call
        handler = Handler.new(event)

        start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC, :float_second)
        results = handler.process

        capture_and_log_errors(results)

        messages = results.transform_values(&:message).compact

        ::Rack::Response.new([JSON.dump(status: :ok, messages: messages)]).finish
      rescue Triage::ClientError => error
        log_error(error)
        error_response(error)
      rescue => error
        log_error(error)
        capture_error(error)
        error_response(error, status: 500)
      ensure
        log_request_processing(results, start_time) if results
      end

      private

      attr_reader :event, :logger

      def log_request_processing(results, start_time)
        logger.info(
          'Event processing',
          dry_run: Triage.dry_run?,
          event_class: event.class,
          event_key: event.key,
          event_payload: event.payload,
          results: results_for_logs(results),
          duration: (Process.clock_gettime(Process::CLOCK_MONOTONIC, :float_second) - start_time).round(5)
        )
      end

      def results_for_logs(results)
        results.each_with_object([]) do |(processor, result), memo|
          memo << { processor: processor, **result.to_h }
        end
      end

      def error_response(error, status: 400)
        ::Rack::Response.new([JSON.dump(status: :error, error: error.class, message: error.message)], status).finish
      end

      def capture_and_log_errors(results)
        results.each do |processor, result|
          next unless result.error

          log_error(result.error, processor)
          capture_error(result.error, processor)
        end
      end

      def capture_error(error, processor = nil)
        Raven.tags_context(object_kind: event.class.to_s)
        Raven.extra_context(processor: processor, payload: event.payload)
        Raven.capture_exception(error)
      end

      def log_error(error, processor = nil)
        logger.error(error, processor: processor)
      end
    end
  end
end
