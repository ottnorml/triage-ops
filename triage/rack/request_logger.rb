# frozen_string_literal: true

require 'rack'
require 'rack/requestid'
require 'time'

module Triage
  module Rack
    # Inspired by https://github.com/eropple/rack-ougai/blob/master/lib/rack/ougai/log_requests.rb
    class RequestLogger < Struct.new(:app)
      def call(env)
        start_time = Time.now
        status, headers, _body = app.call(env)
      ensure
        logger = env[::Rack::RACK_LOGGER]
        logger.info('http', create_log(start_time, env, status, headers))
      end

      private

      def create_log(start_time, env, status, headers)
        end_time = Time.now

        ret = {
          time: start_time.utc,
          # Fix from https://github.com/eropple/rack-ougai/pull/2/files#diff-96b47cd97479144db033cccda03315ffb293d097ae4043c871e896cdbfd191eaR27
          usec: (end_time.to_f - start_time.to_f) * 10**6,
          remote_addr: env['HTTP_X_FORWARDED_FOR'] || env["REMOTE_ADDR"],
          method: env[::Rack::REQUEST_METHOD],
          path: env[::Rack::PATH_INFO],
          query: env[::Rack::QUERY_STRING],
          status: status.to_i
        }

        request_id = env[::Rack::RequestID::REQUEST_ID_KEY]
        ret[:request_id] = request_id unless request_id.nil?

        ret
      end

    end
  end
end
