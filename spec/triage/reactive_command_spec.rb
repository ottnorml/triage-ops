# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/processor'
require_relative '../../triage/triage/reactive_command'

RSpec.describe Triage::ReactiveCommand do
  include_context 'with event', 'Triage::NoteEvent' do
    let(:event_attrs) do
      {
        new_comment: %(@gitlab-bot foo),
      }
    end
  end

  let(:mock) { double('mock', call: true) }
  let(:command_definition) { { name: 'foo' } }
  let(:processor) do
    cmd_definition = command_definition
    Class.new(Triage::Processor) do
      react_to 'merge_request.*'
      define_command cmd_definition

      def initialize(event, mock)
        super(event)
        @mock = mock
      end

      def applicable?
        command.valid?(event)
      end

      def process
        @mock.call(command.args(event))
      end
    end
  end

  subject { processor.new(event, mock) }

  describe 'COMMAND_PREFIX_REGEXP' do
    it { expect(described_class::COMMAND_PREFIX_REGEXP).to eq(/#{Regexp.escape(Triage::GITLAB_BOT)}[[:space:]]+/) }
  end

  describe 'command handling' do
    context 'with no command' do
      before do
        event_attrs[:new_comment] = %(foo bar)
      end

      it 'is not applicable' do
        subject.triage

        expect(mock).not_to have_received(:call)
      end
    end

    context 'with one command' do
      it 'is applicable' do
        subject.triage

        expect(mock).to have_received(:call).once
      end

      context 'when bot is not mentioned' do
        before do
          event_attrs[:new_comment] = %(@user foo)
        end

        it 'is not applicable' do
          subject.triage

          expect(mock).not_to have_received(:call)
        end
      end

      context 'when command is incorrect' do
        before do
          event_attrs[:new_comment] = %(@#{Triage::GITLAB_BOT} bar)
        end

        it 'is not applicable' do
          subject.triage

          expect(mock).not_to have_received(:call)
        end
      end
    end

    context 'with command arguments' do
      before do
        event_attrs[:new_comment] = %(@gitlab-bot foo a b c)
      end

      context 'with args_regex not overidden' do
        it 'uses [] as command arguments' do
          subject.triage

          expect(mock).to have_received(:call).once.with([])
        end
      end

      context 'with args_regex overidden' do
        let(:command_definition) { { name: 'foo', args_regex: %r{~"([^"]+)"|~([^ ]+)} } }

        before do
          event_attrs[:new_comment] = %(Hello\n#{Triage::GITLAB_BOT} foo  ~"group::import"  ~"group::source code"\nWorld!\n#{Triage::GITLAB_BOT} foo ~"group::distribution")
        end

        it 'detects only the first command arguments properly' do
          subject.triage

          expect(mock).to have_received(:call).once.with(['group::import', 'group::source code'])
        end
      end
    end
  end
end
