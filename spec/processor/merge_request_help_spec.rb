# frozen_string_literal: true

require 'spec_helper'

require_relative '../../lib/www_gitlab_com'
require_relative '../../triage/processor/merge_request_help'

RSpec.describe Triage::MergeRequestHelp do
  include_context 'with event', 'Triage::NoteEvent' do
    let(:event_attrs) do
      {
        object_kind: 'note',
        noteable_type: 'merge_request',
        from_gitlab_org?: true,
        wider_community_author?: true,
        by_noteable_author?: true,
        new_comment: %(@gitlab-bot help)
      }
    end
  end

  let(:roulette) do
    [
      { 'username' => 'coach', 'merge_request_coach' => true, 'available' => true },
      { 'username' => 'unavailable_coach', 'merge_request_coach' => true, 'available' => false },
      { 'username' => 'not_coach', 'merge_request_coach' => false, 'available' => true }
    ]
  end

  before do
    allow(WwwGitLabCom).to receive(:roulette).and_return(roulette)
  end

  subject { described_class.new(event) }

  it_behaves_like 'registers listeners', ['merge_request.note']
  it_behaves_like 'command processor', 'help'

  describe '#applicable?' do
    context 'when event is from gitlab org, for a note on a new merge request, by the mr author' do
      include_examples 'event is applicable'
    end

    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event author is not the noteable author' do
      before do
        allow(event).to receive(:by_noteable_author?).and_return(false)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    it 'posts a comment to reply to author and pings MR coaches' do
      body = <<~MARKDOWN.chomp
        Hey there @coach, can you please take a look at this MR and help @root out?

        :wave: @root,

        Thanks for reaching out for help. I've notified [the merge request coaches](https://about.gitlab.com/company/team/?department=merge-request-coach).

        They will get back to you as soon as they can.

        If you have not received any response, you may ask for help again after 1 day.
      MARKDOWN

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end

    it_behaves_like 'rate limited', count: 1, period: 86400
  end
end
