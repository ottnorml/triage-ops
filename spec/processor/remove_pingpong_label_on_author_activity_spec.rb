# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/remove_pingpong_label_on_author_activity'
require_relative '../../triage/triage/event'

RSpec.describe Triage::RemovePingpongLabelOnAuthorActivity do
  include_context 'with event', 'Triage::NoteEvent' do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'update',
        from_gitlab_org?: true,
        by_noteable_author?: true,
        label_names: label_names,
        project_id: 123,
        iid: 300,
      }
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.update', 'merge_request.note']


  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when the event is not by author' do
      before do
        allow(event).to receive(:by_noteable_author?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when there is no pingpong label' do
      let(:label_names) { [''] }

      include_examples 'event is not applicable'
    end

    context 'when there is the pingpong label' do
      let(:label_names) { [described_class::PINGPONG_EMOJI] }

      include_examples 'event is applicable'
    end
  end

  describe '#process' do
    let(:label_names) { [described_class::PINGPONG_EMOJI] }

    it 'posts a message to remove the label' do
      expected_message =<<~MARKDOWN.chomp
        /unlabel ~"#{described_class::PINGPONG_EMOJI}"
      MARKDOWN

      expect_comment_request(event: event, body: expected_message) do
        subject.process
      end
    end
  end
end
