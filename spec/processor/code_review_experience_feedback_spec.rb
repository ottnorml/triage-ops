# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/code_review_experience_feedback'
require_relative '../../triage/triage/event'
require_relative '../../triage/triage'

RSpec.describe Triage::CodeReviewExperienceFeedback do
  include_context 'with event', 'Triage::IssuableEvent' do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'merge',
        from_gitlab_org?: true,
        wider_community_author?: true,
        from_gitlab_com?: false
      }
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.merge', 'merge_request.close']

  describe '#applicable?' do
    context 'when a wider community author' do
      context 'in gitlab-org' do
        before do
          allow(event).to receive(:from_gitlab_org?).and_return(true)
          allow(event).to receive(:from_gitlab_com?).and_return(false)
        end

        include_examples 'event is applicable'
      end

      context 'in gitlab-com' do
        before do
          allow(event).to receive(:from_gitlab_org?).and_return(false)
          allow(event).to receive(:wider_community_author?).and_return(false)
          allow(event).to receive(:from_gitlab_com?).and_return(true)
          allow(event).to receive(:wider_gitlab_com_community_author?).and_return(true)
        end

        include_examples 'event is applicable'
      end
    end

    context 'when not a wider community author' do
      before do
        allow(event).to receive(:wider_community_author?).and_return(false)
        allow(event).to receive(:wider_gitlab_com_community_author?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event is neither from gitlab-org or gitlab-com' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
        allow(event).to receive(:from_gitlab_com?).and_return(false)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    it 'posts code review experience message' do
      body = <<~MARKDOWN.chomp
        #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
        Hi @#{event.resource_author.username},
          
        We would love to know how you found your code review experience in this merge request! Please leave a :thumbsup: or a :thumbsdown: on this comment to describe your experience. 

        Once done, please comment `@gitlab-bot feedback` below and feel free to leave any additional feedback you have in the same comment.

        Thanks for your help! :heart:
      MARKDOWN

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
