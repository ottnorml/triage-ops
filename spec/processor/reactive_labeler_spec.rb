# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/reactive_labeler'

RSpec.describe Triage::ReactiveLabeler do
  include_context 'with event', 'Triage::NoteEvent' do
    let(:event_attrs) do
      {
        object_kind: 'note',
        noteable_type: 'merge_request',
        from_gitlab_org?: true,
        by_noteable_author?: true,
        wider_community_author?: true,
        new_comment: %(#{Triage::GITLAB_BOT} label #{labels})
      }
    end
    let(:labels) { '~"group::import"' }
  end

  subject { described_class.new(event) }

  it_behaves_like 'registers listeners', ['issue.note', 'merge_request.note']
  it_behaves_like 'command processor', 'label', described_class::LABELS_REGEX

  describe 'LABELS_REGEX' do
    it { expect(described_class::LABELS_REGEX).to eq(/~"([^"]+)"|~([^ ]+)/) }
  end

  describe 'ALLOWED_LABELS_REGEX' do
    it { expect(described_class::ALLOWED_LABELS_REGEX).to eq(/\Agroup::[^:]+\z/) }
  end

  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event is for a non-allowed label' do
      context 'when label is ~"pick into x"' do
        let(:labels) { '~"pick into x"' }

        include_examples 'event is not applicable'
      end

      context 'when label is ~"group::import::nested"' do
        let(:labels) { '~"group::import::nested"' }

        include_examples 'event is not applicable'
      end
    end

    context 'when event is for a new merge request mentioning the bot' do
      before do
        allow(event).to receive(:note?).and_return(false)
        allow(event).to receive(:new_entity?).and_return(true)
        allow(event).to receive(:merge_request?).and_return(true)
      end

      context 'when label is ~"group::import"' do
        it_behaves_like 'event is applicable'
      end
    end

    context 'when merge request note is not by the noteable author' do
      before do
        allow(event).to receive(:by_noteable_author?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event is for a new note mentioning the bot' do
      context 'when label is ~"group::import"' do
        it_behaves_like 'event is applicable'
      end
    end
  end

  describe '#process' do
    shared_examples 'message posting' do |expected_labels|
      it 'posts /label command' do
        body = <<~MARKDOWN.chomp
          /label #{expected_labels || labels}
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when event is for a new issue mentioning the bot' do
      before do
        allow(event).to receive(:note?).and_return(false)
        allow(event).to receive(:new_entity?).and_return(true)
        allow(event).to receive(:issue?).and_return(true)
      end

      context 'when label is ~"group::import"' do
        it_behaves_like 'message posting'
      end

      context 'when command is not at the beginning of the new_comment' do
        before do
          allow(event).to receive(:new_comment).and_return(%(Hello\n#{Triage::GITLAB_BOT} label #{labels}\nWorld!))
        end

        it_behaves_like 'message posting'

        context 'with multiple labels on the same line with extra spaces' do
          let(:labels) { '  ~"group::import"   ~"group::source code"   ' }

          it_behaves_like 'message posting', '~"group::import" ~"group::source code"'
        end

        context 'with multiple commands on separate lines' do
          before do
            allow(event).to receive(:new_comment).and_return(%(Hello\n#{Triage::GITLAB_BOT} label #{labels}\nWorld!\n#{Triage::GITLAB_BOT} label ~group::foo))
          end

          it_behaves_like 'message posting', '~"group::import"'
        end
      end
    end

    it_behaves_like 'rate limited'
  end
end
