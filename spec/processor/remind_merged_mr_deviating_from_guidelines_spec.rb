# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/remind_merged_mr_deviating_from_guidelines'
require_relative '../../triage/triage/event'

RSpec.describe Triage::RemindMergedMrDeviatingFromGuideline do
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      {
        from_gitlab_org_gitlab?: true,
        project_id: project_id,
        iid: iid
      }
    end
    let(:project_id) { 123 }
    let(:iid) { 456 }
    let(:latest_pipeline_id) { 42 }
    let(:latest_pipeline_ref) { "refs/merge-requests/#{iid}/merge" }
    let(:latest_pipeline_status) { 'success' }
    let(:latest_pipeline_project_id) { project_id }
    let(:latest_pipeline_finished_at) { Time.now - 3600 }
    let(:latest_pipeline) do
      {
        id: latest_pipeline_id,
        ref: latest_pipeline_ref,
        status: latest_pipeline_status,
        project_id: latest_pipeline_project_id,
        finished_at: latest_pipeline_finished_at
      }
    end
    let(:pipelines) do
      [
        latest_pipeline,
        {
          id: 12
        }
      ]
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.merge']

  describe '#applicable?' do
    before do
      allow(event).to receive(:with_project_id?).and_return(true)
    end

    context 'when event project is not under gitlab-org/gitlab' do
      before do
        allow(event).to receive(:from_gitlab_org_gitlab?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'with API call for pipelines' do
      before do
        stub_api_request(path: "/projects/#{project_id}/merge_requests/#{iid}/pipelines", response_body: pipelines)
        stub_api_request(path: "/projects/#{project_id}/pipelines/#{latest_pipeline_id}", response_body: latest_pipeline)
      end

      context 'and latest pipeline from canonical project and latest pipeline is recent enough' do
        include_examples 'event is not applicable'
      end

      context 'and latest pipeline not from canonical project' do
        let(:latest_pipeline_project_id) { 999 }

        before do
          # Ideally we'd use a partial double for event so that we don't have to stub this method
          expect(event).to receive(:with_project_id?).with(latest_pipeline_project_id).and_return(false)
        end

        context 'and latest pipeline still running' do
          let(:latest_pipeline_status) { 'running' }
          let(:latest_pipeline_finished_at) { nil }

          include_examples 'event is applicable'
        end

        context 'and latest pipeline recent enough' do
          include_examples 'event is applicable'
        end

        context 'and latest pipeline ref is not merged results' do
          let(:latest_pipeline_ref) { "refs/merge-requests/#{iid}/head" }

          include_examples 'event is applicable'
        end

        context 'and latest pipeline is not recent enough' do
          let(:latest_pipeline_finished_at) { Time.now - 3600 * (described_class::HOURS_THRESHOLD_FOR_STALE_MR_PIPELINE + 1) }

          include_examples 'event is applicable'
        end
      end

      context 'and latest pipeline from canonical project' do
        context 'and latest pipeline still running' do
          let(:latest_pipeline_status) { 'running' }
          let(:latest_pipeline_finished_at) { nil }

          include_examples 'event is not applicable'
        end

        context 'and latest pipeline recent enough' do
          include_examples 'event is not applicable'
        end

        context 'and latest pipeline ref is not merged results' do
          let(:latest_pipeline_ref) { "refs/merge-requests/#{iid}/head" }

          include_examples 'event is applicable'
        end

        context 'and latest pipeline is not recent enough' do
          let(:latest_pipeline_finished_at) { Time.now - 3600 * (described_class::HOURS_THRESHOLD_FOR_STALE_MR_PIPELINE + 1) }

          include_examples 'event is applicable'
        end
      end
    end
  end

  describe '#process' do
    it 'calls remind_merged_mr_deviating_from_guidelines' do
      expect(subject).to receive(:remind_merged_mr_deviating_from_guidelines)

      subject.process
    end

    it 'posts a comment' do
      body = <<~MARKDOWN.chomp
        @#{event.event_user_username}, did you forget to run a pipeline before you merged this work? Based on our [code review process](https://docs.gitlab.com/ee/development/code_review.html#merging-a-merge-request), if the latest pipeline is more than 2 hours old, you should:

        1. Ensure the merge request is not in Draft status.
        1. Start a pipeline. (Especially important for ~"Community contribution" merge requests.)
        1. Set the merge request to merge when pipeline succeeds.

        ([Improve this message?](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/processor/remind_merged_mr_deviating_from_guidelines.rb))
      MARKDOWN

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
