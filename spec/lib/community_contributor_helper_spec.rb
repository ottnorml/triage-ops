# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/community_contribution_helper'

RSpec.describe CommunityContributionHelper do
  let(:resource_klass) do
    Struct.new(:author) do
      include CommunityContributionHelper
    end
  end

  let(:author) { 'community-member' }

  subject { resource_klass.new(author) }

  describe '#bot_author?' do
    context 'when author is not a bot' do
      let(:author) { 'community-member' }

      it 'returns false' do
        expect(subject.bot_author?).to be_falsey
      end
    end

    project_bot_names = %w[project_123_bot project_123_bot12]

    project_bot_names.each do |project_bot_name|
      context "when author is a project bot user #{project_bot_name}" do
        let(:author) { project_bot_name }

        it 'returns true' do
          expect(subject.bot_author?).to be_truthy
        end
      end
    end

    context 'when author is a known bot' do
      let(:author) { described_class::GITLAB_DEPENDENCY_UPDATE_BOT }

      it 'returns true' do
        expect(subject.bot_author?).to be_truthy
      end
    end
  end
end
