# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/untriaged_helper'

RSpec.describe UntriagedHelper do
  let(:resource_klass) do
    Struct.new(:labels) do
      include UntriagedHelper
    end
  end

  let(:label_klass) do
    Struct.new(:name)
  end

  let(:labels) { [] }

  let(:team_from_www) do
    {
      'user1' => { 'departments' => ['Quality Department', 'Merge Request coach'] },
      'user2' => { 'departments' => ['Quality Department', 'Merge Request coach'] },
      'user3' => { 'departments' => ['Quality Department'] },
      'user4' => { 'role' => 'Director of Quality', 'departments' => ['Quality Department'] },
      'user5' => { 'role' => 'Senior Operations Analyst', 'departments' => ['Quality Department'] },
      'user6' => { 'role' => 'Staff Infrastructure Analyst', 'departments' => ['Quality Department'] },
      'user7' => { 'role' => 'Vice President of Quality', 'departments' => ['Quality Department'] },
      'user8' => { 'role' => 'Engineering Manager, Engineering Productivity', 'departments' => ['Quality Department'] },
      'user9' => { 'role' => 'Quality Engineering Manager, Engineering Productivity', 'departments' => ['Quality Department'] }
    }
  end

  let(:roulette) do
    [
      { 'username' => 'user1' },
      { 'username' => 'user2', 'out_of_office' => true },
      { 'username' => 'user3', 'out_of_office' => false },
      { 'username' => 'user4', 'out_of_office' => false }
    ]
  end

  subject { resource_klass.new(labels) }

  describe '#untriaged_issues_triagers' do
    it 'retrieves triage team from www-gitlab-com' do
      allow(WwwGitLabCom).to receive(:team_from_www).and_return(team_from_www)
      allow(WwwGitLabCom).to receive(:roulette).and_return(roulette)

      expect(subject.untriaged_issues_triagers).to match_array(%w[@user8 @user9])
    end
  end

  describe '#merge_request_coaches' do
    it 'retrieves merge request coaches from www-gitlab-com' do
      allow(WwwGitLabCom).to receive(:team_from_www).and_return(team_from_www)
      allow(WwwGitLabCom).to receive(:roulette).and_return(roulette)

      expect(subject.merge_request_coaches).to match_array(%w[@user1])
    end
  end

  describe '#distribute_items' do
    let(:list_items) { (1..7).to_a.map { |i| "Item ##{i}" } }
    let(:potential_triagers) { %w[@triager-3 @triager-2 @triager-1] }

    before do
      allow(subject).to receive(:puts)
    end

    it 'distributes items all items across triagers' do
      distribution = subject.distribute_items(list_items, potential_triagers)
      items = distribution.values.inject(&:+)

      expect(items).to match_array(list_items)
    end

    it 'sorts triagers by username' do
      distribution = subject.distribute_items(list_items, potential_triagers)
      triagers = distribution.keys

      expect(triagers).to eq(potential_triagers.sort)
    end
  end

  describe '#untriaged?' do
    context 'for triaged resource' do
      let(:labels) { [label_klass.new('group::runner'), label_klass.new('devops::verify'), label_klass.new('type::bug')] }

      it 'returns false' do
        expect(subject.untriaged?).to eq(false)
      end
    end

    context 'for untriaged resource' do
      context 'no stage label' do
        let(:labels) { [label_klass.new('group::runner'), label_klass.new('type::bug')] }

        it 'returns true' do
          expect(subject.untriaged?).to eq(true)
        end
      end

      context 'no group label' do
        let(:labels) { [label_klass.new('devops::verify'), label_klass.new('type::bug')] }

        it 'returns true' do
          expect(subject.untriaged?).to eq(true)
        end
      end

      context 'no type label' do
        let(:labels) { [label_klass.new('group::runner'), label_klass.new('devops::verify')] }

        it 'returns true' do
          expect(subject.untriaged?).to eq(true)
        end
      end
    end

    context 'for unlabelled resource' do
      it 'returns true' do
        expect(subject.untriaged?).to eq(true)
      end
    end
  end

  describe '#merge_requests_from_line_items' do
    let(:items) do
      [
        '@user1,https://gitlab/merge-request/1,label1',
        '@user2,https://gitlab/merge-request/2,label2,label3',
        '@user1,https://gitlab/merge-request/3,label1',
      ].join("\n")
    end

    it 'returns list of merge requests' do
      expected = [
        { author: '@user1', url: 'https://gitlab/merge-request/1', labels: ['label1'] },
        { author: '@user2', url: 'https://gitlab/merge-request/2', labels: ['label2', 'label3'] },
        { author: '@user1', url: 'https://gitlab/merge-request/3', labels: ['label1'] }
      ]

      expect(subject.merge_requests_from_line_items(items)).to eq(expected)
    end
  end

  describe '#tabulate_merge_requests_by_author' do
    let(:merge_requests) do
      [
        { author: '@user1', url: 'https://gitlab/merge-request/1', labels: ['label1'] },
        { author: '@user2', url: 'https://gitlab/merge-request/2', labels: ['label2', 'label3'] },
        { author: '@user1', url: 'https://gitlab/merge-request/3', labels: ['label3'] }
      ]
    end

    it 'returns a table containing merge requests per author' do
      expected = <<~TABLE
        | Author | Merge Request URLs |
        | --- | --- |
        | @user1 | <ol><li>https://gitlab/merge-request/1: label1</li><li>https://gitlab/merge-request/3: label3</li></ol> |
        | @user2 | <ol><li>https://gitlab/merge-request/2: label2 label3</li></ol> |
      TABLE

      expect(subject.tabulate_merge_requests_by_author(merge_requests)).to eq(expected)
    end
  end
end
