# frozen_string_literal: true

require 'spec_helper'
require 'active_support'
require 'active_support/core_ext/numeric/time'
require_relative '../../lib/stale_resources_helper'

RSpec.describe StaleResources do
  let(:project_path) { 'my_project' }
  let(:token) { 'token' }
  let(:project_id) { 123 }
  let(:resource_iid) { 345 }
  let(:days) { 365 }

  let(:stale_notes) do
    [
      {
        'author' => {
          'username' => 'user'
        },
        'updated_at' => (days + 1).days.ago.to_s
      }
    ]
  end
  let(:active_notes) do
    [
      {
        'author' => {
          'username' => 'user'
        },
        'updated_at' => 1.days.ago.to_s
      }
    ]
  end

  let(:notes_in_use) { stale_notes }

  let(:issue_notes) { notes_in_use }
  let(:merge_request_notes) { notes_in_use}

  let(:old_resource) do
    {
      'created_at' => (days + 2).days.ago.to_s
    }
  end
  let(:new_resource) do
    {
      'created_at' => 2.days.ago.to_s
    }
  end

  let(:resource_in_use) { old_resource }

  let(:default_args) do
    {
      project_path: project_path,
      token: token,
      project_id: project_id,
      resource_iid: resource_iid,
      days: days
    }
  end

  before do
    allow(Gitlab).to receive(:client).and_return(
      double(
        issue_notes: issue_notes,
        merge_request_notes: merge_request_notes,
        issue: resource_in_use,
        merge_request: resource_in_use
      )
    )
  end

  context 'class methods' do
    [:issue, :merge_request].each do |res_type|
      let(:args) { default_args.merge(resource_type: res_type) }

      describe '.stale?' do
        context 'with notes' do
          context 'for old notes' do
            it 'returns true' do
              expect(described_class.stale?(args)).to be_truthy
            end
          end

          context 'for new notes' do
            let(:notes_in_use) { active_notes }

            it 'returns false' do
              expect(described_class.stale?(args)).to be_falsey
            end
          end
        end

        context 'without notes' do
          let(:notes_in_use) { [] }

          context 'for old resource' do
            it 'returns true' do
              expect(described_class.stale?(args)).to be_truthy
            end
          end

          context 'for new resource' do
            let(:resource_in_use) {new_resource}

            it 'returns false' do
              expect(described_class.stale?(args)).to be_falsey
            end
          end
        end
      end

      describe '.active?' do
        context 'with notes' do
          context 'for old notes' do
            it 'returns false' do
              expect(described_class.active?(args)).to be_falsey
            end
          end

          context 'for new notes' do
            let(:notes_in_use) { active_notes }

            it 'returns true' do
              expect(described_class.active?(args)).to be_truthy
            end
          end
        end

        context 'without notes' do
          let(:notes_in_use) { [] }

          context 'for old resource' do
            it 'returns false' do
              expect(described_class.active?(args)).to be_falsey
            end
          end

          context 'for new resource' do
            let(:resource_in_use) {new_resource}

            it 'returns true' do
              expect(described_class.active?(args)).to be_truthy
            end
          end
        end
      end
    end
  end

  describe '#days_since_last_human_update' do
    subject(:days_since_last_human_update) { described_class.new(**args.except(:days)).days_since_last_human_update }

    [:issue, :merge_request].each do |res_type|
      let(:args) { default_args.merge(resource_type: res_type) }

      context 'when there are not recent notes' do
        let(:notes_in_use) { stale_notes }

        it 'returns 366' do
          expect(days_since_last_human_update).to eq(366)
        end
      end

      context 'when there is a recent note' do
        let(:notes_in_use) { [*stale_notes, *active_notes] }

        it 'returns 1' do
          expect(days_since_last_human_update).to eq(366)
        end
      end
    end
  end
end
