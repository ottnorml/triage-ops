
# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/threat_insights_helper'

RSpec.describe ThreatInsightsHelper do
  let(:resource_klass) do
    Struct.new(:labels) do
      include ThreatInsightsHelper
    end
  end

  let(:label_klass) do
    Struct.new(:name)
  end

  let(:labels) { [] }

  let(:team_from_www) do
    {
      'user1' => { 'departments' => ['Secure:Threat Insights BE Team'] },
      'user2' => { 'departments' => ['Secure:Threat Insights FE Team'] },
      'user3' => { 'departments' => ['Secure:Threat Insights BE Team'] },
      'user4' => { 'departments' => ['Secure:Threat Insights FE Team'] }
    }
  end

  before do
    allow(WwwGitLabCom).to receive(:team_from_www).and_return(team_from_www)
    allow(WwwGitLabCom).to receive(:roulette).and_return([])
  end

  subject { resource_klass.new(labels) }

  describe '#threat_insights_be' do
    it 'retrieves team members from www-gitlab-com and returns a random threat insights backend engineer' do
      expect(%w(@user1 @user3)).to include(subject.threat_insights_be)
    end
  end

  describe '#threat_insights_fe' do
    it 'retrieves team members from www-gitlab-com and returns a random threat insights frontend engineer' do
      expect(%w(@user2 @user4)).to include(subject.threat_insights_fe)
    end
  end
end
