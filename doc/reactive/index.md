# Reactive operations

This project runs a Ruby web service (a Rack application) which receives and react to webhooks from [gitlab-org group](https://gitlab.com/gitlab-org).

For every webhook request, the service passes the event payload to each processors sequentially.

The list of processors can be found under [`triage/processor/`](triage/processor/).

## Specific topics

- [Architecture](./architecture.md)
- [Run locally](./run_locally.md)
- [Implementing a new processor](./processor.md)
- [Production](./production.md)
