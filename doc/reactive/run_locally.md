# Running locally

1. In the project directory, build the docker image. This creates a docker image named `triage-ops`:
    ```
    docker build --file=Dockerfile.rack -t triage-ops .
    ```

1. Run the server locally (here, in dry mode) using Docker, passing required environment variables.
    ```
    docker run -itp 8080:8080 \
      -e GITLAB_API_ENDPOINT=http://localhost:3000/api/v4 \
      -e GITLAB_WEBHOOK_TOKEN=gitlab_webhook_token \
      -e GITLAB_API_TOKEN=gitlab_api_token \
      -e SLACK_WEBHOOK_URL=https://example.org \
      -e DRY_RUN=1 \
      triage-ops
    ```

1. Test the endpoint
    ```
    curl -X POST 0.0.0.0:8080 -H "Content-Type: application/json" -H "X-Gitlab-Token: gitlab_webhook_token" -d @spec/fixture/gitlab_test_note.json
    ```

## Running with a local GitLab instance

You can run `triage-ops` with a local GitLab instance (e.g GDK) and have it react to events coming from the local instance.

There are some prerequisites you would need in the local GitLab instance:

1. The following groups and subgroups:
    - `gitlab-org`
    - `gitlab-com`
    - `gitlab-org/gitlab-core-team/community-members`
1. The following projects:
    - `gitlab-org/gitlab`
1. A `bot` user who is a member of `gitlab-org` and `gitlab-com` groups, with an api scoped access token. This access token should be set in the environment `GITLAB_API_TOKEN` when running `triage-ops`.
1. Enable a group webhook for the `gitlab-org` and `gitlab-com` groups that points to `http://0.0.0.0:8080`
    - Set a value for the `Secret token` field of the webhook. This secret should be set in the environment `GITLAB_WEBHOOK_TOKEN` when running `triage-ops`.
1. Enable outbound webhook request to `localhost`. See [documentation](https://docs.gitlab.com/ee/security/webhooks.html) for detail.

Once the prerequisites are all set, you can start the server locally as shown in the [Running as a local server](#running-as-a-local-server) section, but not in dry-run mode this time:

```
docker run -itp 8080:8080 \
  -e GITLAB_API_ENDPOINT=http://localhost:3000/api/v4 \
  -e GITLAB_WEBHOOK_TOKEN=gitlab_webhook_token \
  -e GITLAB_API_TOKEN=gitlab_api_token \
  -e SLACK_WEBHOOK_URL=https://example.org \
  triage-ops
```

TODO:
- [ ] Stub Slack webhook URL
