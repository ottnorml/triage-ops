# frozen_string_literal: true

require 'yaml'
require 'erb'
require 'fileutils'

module Generate
  module CiJob
    GROUP_DATA = YAML.load_file(File.expand_path("#{__dir__}/../../group-definition.yml")).freeze

    Group = Struct.new(:name, :definition, :policy_file) do
      def default_labeling
        (definition['default_labeling'] || {}).slice('groups', 'projects')
      end
    end

    def self.run(options)
      template_name = File.basename(options.template)
      erb = ERB.new(File.read(options.template), trim_mode: '-')
      policy_base = "policies/generated/#{template_name}"

      FileUtils.rm_rf(File.join(destination, "#{template_name}.yml"), secure: true)
      FileUtils.mkdir_p(destination)

      policy_file_names = Dir["#{policy_base}/*.{yml,yaml}"]
        .map(&File.method(:basename)).sort

      teams = policy_file_names.map do |file_name|
        name = File.basename(file_name, '.*')
        Group.new(name, GROUP_DATA[name], "#{policy_base}/#{file_name}")
      end

      if teams.any?
        File.write(
          "#{destination}/#{template_name}.yml",
          erb.result_with_hash(teams: teams).squeeze("\n")
        )
      end
    end

    def self.destination
      @destination ||=
        File.expand_path('generated', "#{__dir__}/../../.gitlab/ci")
    end
  end
end
